package com.falabella.app;

import java.sql.*;
import java.util.Arrays;

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.io.*;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.Option.Builder;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.ParseException;

public class OracleConnDate {

	public static void main(String args[]) {

		String CONN_DBURL = "";
		String CONN_DBPORT = "";
		String CONN_DBSCHEMA = "";
		// Calculated

		// File parameters
		String CSV_SEP = "";
		// Encryption
		final String salt = "08c92cfa3745bcdb";
		final String UNIX_EOL = "\n";

		
		String CONN_USER = "";
		String CONN_PASSWORD = "";
		String TABLE_NAME = "";
		String DATE_INDEX = "";
		String columnsToEncrypt = "";
		String DATE="";
		String password = "";
		
		Boolean flag = false;
		String CONN_STRING ="";
		
		
		if(args.length>10) {
			CONN_USER=args[0];
			CONN_PASSWORD=args[1];
			CONN_DBURL=args[2];
			CONN_DBPORT=args[3];
			CONN_DBSCHEMA=args[4];
			TABLE_NAME=args[5];
			DATE_INDEX=args[6];
			DATE=args[7];
			columnsToEncrypt=args[8];
			password=args[9];
			CSV_SEP=args[10];
			
			CONN_STRING = "jdbc:oracle:thin:@" + CONN_DBURL + ":" + CONN_DBPORT + "/" + CONN_DBSCHEMA;

			System.out.println("Running with user:            "+CONN_USER);
			System.out.println("Running with password          ***********");
			System.out.println("Running with table:           "+TABLE_NAME);
			System.out.println("Running with encryption cols: "+DATE_INDEX);
			System.out.println("Running with date index       "+columnsToEncrypt);
			System.out.println("Running with date value:      "+DATE);
			System.out.println("Got password");
			flag=true;
		}		else {
			System.out.println("Usage:");
			System.out.println("java -jar select_date.jar "
					+ "dbuser dbpassword dburl dbport dbschema "
					+ "table date_index date columns_to_encrypt encryption_password separator");
		}
		

		if (flag) {

			try {

				Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection con = DriverManager.getConnection(CONN_STRING, CONN_USER, CONN_PASSWORD);
				// Setting auto commit for enabling streaming
				con.setAutoCommit(false);
				// Setting statement size to retrieve just 100 records per DBcall
				Statement stmt = con.createStatement();
				stmt.setFetchSize(100);

				String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + DATE_INDEX + " = TO_DATE('" + DATE
						+ "', 'yyyy-mm-dd' )";
				String outputFile = TABLE_NAME + "_" + DATE + "_encrypted.gz";
				String[] columnsToEncryptArray = columnsToEncrypt.split(",");
				OutputStream fout = new FileOutputStream(outputFile);
				OutputStream gzip = new GzipCompressorOutputStream(fout);
				TextEncryptor encryptor = Encryptors.queryableText(password, salt);

				System.out.println(query);
				ResultSet rs = stmt.executeQuery(query);
				ResultSetMetaData metadata = rs.getMetaData();
				int numColumns = metadata.getColumnCount();
				System.out.println("Detected columns: " + numColumns);
				int numRows = 0;

				// Configure header
				String header = "";

				// Create headers and datatype

				for (int column = 1; column <= numColumns; column++) {
					System.out.println("\t Column:  " + metadata.getColumnLabel(column));
					System.out.println("\t Type:    " + metadata.getColumnTypeName(column));
					System.out.println("\t Encrypt: "
							+ String.valueOf(Arrays.asList(columnsToEncryptArray).contains(String.valueOf(column))));
					String column_type = metadata.getColumnTypeName(column);
					String column_name = metadata.getColumnLabel(column);
					// Force string to Encrypted columns
					if (Arrays.asList(columnsToEncryptArray).contains(String.valueOf(column)))
						column_type = "VARCHAR2";
					if (column == numColumns) {
						header = header + column_name + "_" + column_type;
					} else {
						header = header + column_name + "_" + column_type + CSV_SEP;
					}
				}

				System.out.println("Writing header...");
				System.out.println("\t File:    " + outputFile);
				header = header + UNIX_EOL;
				gzip.write(header.getBytes());
				while (rs.next()) // iterate rows
				{
					++numRows;
					String linea = "";
					for (int i = 1; i <= numColumns; ++i) // iterate columns
					{
						String linea_raw = String.valueOf(rs.getObject(i)).replaceAll(CSV_SEP, "");

						linea_raw = linea_raw.replaceAll("[^\\x00-\\x7F]", "");

						// erases all the ASCII control characters by space
						linea_raw = linea_raw.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", " ");
						
						// removes non-printable characters from Unicode
						linea_raw = linea_raw.replaceAll("\\p{C}", "");
						// removes non latin N characters from Unicode
						linea_raw = linea_raw.replaceAll("\\u00f1", "\\u000f");
						linea_raw = linea_raw.replaceAll("\\u00d1", "\\u000d");
						// removes pipe characters from Unicode with not separator
						linea_raw = linea_raw.replaceAll("\\u007C", "");
						linea_raw = linea_raw.replaceAll(",", "");
						
						if (Arrays.asList(columnsToEncryptArray).contains(String.valueOf(i)))
							linea_raw = encryptor.encrypt(linea_raw);
						
						
						
						if (i == numColumns)
							linea = linea + linea_raw;
						else
							linea = linea + linea_raw + CSV_SEP;
					}
					linea = linea + UNIX_EOL;
					gzip.write(linea.getBytes());
				}

				System.out.println("Finished: " + numRows + " Rows");
				gzip.close();
				con.close();
			} catch (Exception e) {
				System.out.println(e);
			}//catch inside
		}// end-if-flag

	}//end-main
}//end-class

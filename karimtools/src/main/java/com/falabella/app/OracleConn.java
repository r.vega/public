package com.falabella.app;

import java.sql.*;
import java.util.Arrays;

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.io.*;

public class OracleConn {

	public static void main(String args[]) {
		
		String CONN_DBURL ="";
		String CONN_DBPORT ="";
		String CONN_DBSCHEMA = "";
		String CSV_SEP = "\t";
		// Encryption
		final String salt = "08c92cfa3745bcdb";
		final String UNIX_EOL = "\n";

		Boolean flag = false;
		
		String CONN_USER = "";
		String CONN_PASSWORD = "";
		String TABLE_NAME = "";
		String columnsToEncrypt = "";
		String password = "";
		
		
		if(args.length>8) {
			CONN_USER=args[0];
			CONN_PASSWORD=args[1];
			CONN_DBURL=args[2];
			CONN_DBPORT=args[3];
			CONN_DBSCHEMA=args[4];
			TABLE_NAME=args[5];
			columnsToEncrypt=args[6];
			password=args[7];
			CSV_SEP=args[8];
			System.out.println("Running with user:             "+CONN_USER);
			System.out.println("Running with password          ***********");
			System.out.println("Running with table:            "+TABLE_NAME);
			System.out.println("Running with colums to encrypt "+columnsToEncrypt);
			System.out.println("Got password");
			flag=true;
		}else {
			System.out.println("Usage:");
			System.out.println("java -jar select_all.jar "
					+ "dbuser dbpassword dburl dbport dbschema "
					+ "table columns_to_encrypt encryption_password separator");
		}
		
		

		final String CONN_STRING = "jdbc:oracle:thin:@" + CONN_DBURL + ":" + CONN_DBPORT + "/" + CONN_DBSCHEMA;

		if (flag) {
			try {

				// Resultset documentation
				// https://docs.oracle.com/javase/7/docs/api/java/sql/ResultSetMetaData.html

				Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection con = DriverManager.getConnection(CONN_STRING, CONN_USER, CONN_PASSWORD);
				// Setting auto commit for enabling streaming
				con.setAutoCommit(false);
				// Setting statement size to retrieve just 100 records per DBcall
				Statement stmt = con.createStatement();
				stmt.setFetchSize(100);


				String query = "SELECT * FROM " + TABLE_NAME;
				String outputFile = TABLE_NAME + "_encrypted.gz";

				String[] columnsToEncryptArray = columnsToEncrypt.split(",");

				OutputStream fout = new FileOutputStream(outputFile);
				OutputStream gzip = new GzipCompressorOutputStream(fout);
				TextEncryptor encryptor = Encryptors.queryableText(password, salt);

				System.out.println(query);

				ResultSet rs = stmt.executeQuery(query);

				ResultSetMetaData metadata = rs.getMetaData();

				int numColumns = metadata.getColumnCount();
				System.out.println("Detected columns: " + numColumns);
				int numRows = 0;

				// Configure header
				String header = "";

				// Create headers and datatype

				for (int column = 1; column <= numColumns; column++) {
					System.out.println("\t Column:  " + metadata.getColumnLabel(column));
					System.out.println("\t Type:    " + metadata.getColumnTypeName(column));
					System.out.println("\t Encrypt: "
							+ String.valueOf(Arrays.asList(columnsToEncryptArray).contains(String.valueOf(column))));

					String column_type = metadata.getColumnTypeName(column);
					String column_name = metadata.getColumnLabel(column);

					// Force string to Encrypted columns
					if (Arrays.asList(columnsToEncryptArray).contains(String.valueOf(column)))
						column_type = "VARCHAR2";

					if (column == numColumns) {
						header = header + column_name + "_" + column_type;
					} else {
						header = header + column_name + "_" + column_type + CSV_SEP;
					}
				}

				System.out.println("Writing header...");
				System.out.println("\t File:    " + outputFile);
				header = header + UNIX_EOL;

				gzip.write(header.getBytes());
				while (rs.next()) // iterate rows
				{
					++numRows;
					String linea = "";

					for (int i = 1; i <= numColumns; ++i) // iterate columns
					{
						String linea_raw = String.valueOf(rs.getObject(i)).replaceAll(CSV_SEP, "");
						
						linea_raw = linea_raw.replaceAll("[^\\x00-\\x7F]", "");

						// erases all the ASCII control characters by space
						linea_raw = linea_raw.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", " ");
						
						// removes non-printable characters from Unicode
						linea_raw = linea_raw.replaceAll("\\p{C}", "");
						// removes non latin N characters from Unicode
						linea_raw = linea_raw.replaceAll("\\u00f1", "\\u000f");
						linea_raw = linea_raw.replaceAll("\\u00d1", "\\u000d");
						// removes pipe characters from Unicode with not separator
						linea_raw = linea_raw.replaceAll("\\u007C", "");
						linea_raw = linea_raw.replaceAll(",", "");

						if (Arrays.asList(columnsToEncryptArray).contains(String.valueOf(i)))
							linea_raw = encryptor.encrypt(linea_raw);

						if (i == numColumns)
							linea = linea + linea_raw;
						else
							linea = linea + linea_raw + CSV_SEP;
					}

					linea = linea + UNIX_EOL;
					gzip.write(linea.getBytes());

				}

				System.out.println("Finished: " + numRows + " Rows");
				gzip.close();
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

}

package com.falabella.app;

import java.sql.*;
import java.util.Arrays;

import org.apache.commons.compress.compressors.gzip.GzipCompressorOutputStream;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;

import java.io.*;

public class OracleConnString {

	public static void main(String args[]) {

		// Connection parameters
		/*
		final String CONN_DBURL = "marea-cluster.falabella.cl";
		final String CONN_DBPORT = "1531";
		final String CONN_DBSCHEMA = "dbmk_app";
		final String CSV_SEP = "\t";
		*/
		
		String CONN_DBURL ="";
		String CONN_DBPORT ="";
		String CONN_DBSCHEMA = "";
		String CSV_SEP = "\t";
		// Encryption
		final String salt = "08c92cfa3745bcdb";
		final String UNIX_EOL = "\n";

		Boolean flag = false;
		
		String CONN_USER = "";
		String CONN_PASSWORD = "";
		String TABLE_NAME = "";
		String columnsToEncrypt = "";
		String password = "";
		String CUSTOM_QUERY="";
		//String password = "f4l4b3114";
		
		
		if(args.length>9) {
			CONN_USER=args[0];
			CONN_PASSWORD=args[1];
			CONN_DBURL=args[2];
			CONN_DBPORT=args[3];
			CONN_DBSCHEMA=args[4];
			TABLE_NAME=args[5];
			CUSTOM_QUERY=args[6];
			columnsToEncrypt=args[7];
			password=args[8];
			CSV_SEP=args[9];
			System.out.println("Running with user:             "+CONN_USER);
			System.out.println("Running with password          ***********");
			System.out.println("Running with table:            "+TABLE_NAME);
			System.out.println("Running with colums to encrypt "+columnsToEncrypt);
			System.out.println("Running with custom Query: "+CUSTOM_QUERY);
			System.out.println("Got password");
			flag=true;
		}else {
			System.out.println("Usage:");
			System.out.println("java -jar oracle_custom.jar "
					+ "dbuser dbpassword dburl dbport dbschema "
					+ "table custom_query columns_to_encrypt encryption_password separator");
		}
		
		

		final String CONN_STRING = "jdbc:oracle:thin:@" + CONN_DBURL + ":" + CONN_DBPORT + "/" + CONN_DBSCHEMA;

		if (flag) {
			try {

				// Resultset documentation
				// https://docs.oracle.com/javase/7/docs/api/java/sql/ResultSetMetaData.html

				Class.forName("oracle.jdbc.driver.OracleDriver");
				Connection con = DriverManager.getConnection(CONN_STRING, CONN_USER, CONN_PASSWORD);
				// Setting auto commit for enabling streaming
				con.setAutoCommit(false);
				// Setting statement size to retrieve just 100 records per DBcall
				Statement stmt = con.createStatement();
				stmt.setFetchSize(100);

				/*
				 * FT_BOL_VENTA String TABLE_NAME = "DBMARK.FT_BOL_VENTA"; String YYYY = "2018";
				 * String MM = "11"; String DD = "1";
				 * 
				 * String query = "SELECT * FROM " + TABLE_NAME +
				 * " WHERE EXTRACT(YEAR FROM ID_DIA)="+YYYY+
				 * " AND EXTRACT(MONTH FROM ID_DIA)="+MM+ " AND EXTRACT(DAY FROM ID_DIA)="+DD;
				 */

				/* 26,27,31,32 */


				String query = CUSTOM_QUERY;
				

				String[] columnsToEncryptArray = columnsToEncrypt.split(",");


				TextEncryptor encryptor = Encryptors.queryableText(password, salt);

				System.out.println(query);

				ResultSet rs = stmt.executeQuery(query);

				ResultSetMetaData metadata = rs.getMetaData();

				int numColumns = metadata.getColumnCount();
				System.out.println("Detected columns: " + numColumns);
				int numRows = 0;

				// Configure header
				String header = "";

				// Create headers and datatype
				String partPattern = "part";
				String outputFile = TABLE_NAME + "_encrypted.gz";
				OutputStream fout = new FileOutputStream(outputFile);
				OutputStream gzip = new GzipCompressorOutputStream(fout);
				
				
				for (int column = 1; column <= numColumns; column++) {
					System.out.println("\t Column:  " + metadata.getColumnLabel(column));
					System.out.println("\t Type:    " + metadata.getColumnTypeName(column));
					System.out.println("\t Encrypt: "
							+ String.valueOf(Arrays.asList(columnsToEncryptArray).contains(String.valueOf(column))));

					String column_type = metadata.getColumnTypeName(column);
					String column_name = metadata.getColumnLabel(column);

					// Force string to Encrypted columns
					if (Arrays.asList(columnsToEncryptArray).contains(String.valueOf(column)))
						column_type = "VARCHAR2";

					if (column == numColumns) {
						header = header + column_name + "_" + column_type;
					} else {
						header = header + column_name + "_" + column_type + CSV_SEP;
					}
				}

				System.out.println("Writing header...");
				System.out.println("\t File:    " + outputFile);
				header = header + UNIX_EOL;

				gzip.write(header.getBytes());
				while (rs.next()) // iterate rows
				{
					++numRows;
					String linea = "";

					for (int i = 1; i <= numColumns; ++i) // iterate columns
					{
						String linea_raw = String.valueOf(rs.getObject(i)).replaceAll(CSV_SEP, "");
						
						linea_raw = linea_raw.replaceAll("[^\\x00-\\x7F]", "");

						// erases all the ASCII control characters by space
						linea_raw = linea_raw.replaceAll("[\\p{Cntrl}&&[^\r\n\t]]", " ");
						
						// removes non-printable characters from Unicode
						linea_raw = linea_raw.replaceAll("\\p{C}", "");

						if (Arrays.asList(columnsToEncryptArray).contains(String.valueOf(i)))
							linea_raw = encryptor.encrypt(linea_raw);

						if (i == numColumns)
							linea = linea + linea_raw;
						else
							linea = linea + linea_raw + CSV_SEP;
					}

					linea = linea + UNIX_EOL;
					gzip.write(linea.getBytes());

				}

				System.out.println("Finished: " + numRows + " Rows");
				gzip.close();
				con.close();

			} catch (Exception e) {
				System.out.println(e);
			}
		}
	}

}

package com.falabella.app;

import java.io.*;
 
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
 
public class JsonCrypt {


    public static void main( String[] args )
    {
    	
    	Gson gson = new GsonBuilder().create();
    	// place store this guys in another place
        final String password = "falabellarocks";  
        final String salt = "08c92cfa3747bcdb";
        //final String salt = KeyGenerators.string().generateKey();

		try {       

                    // TODO: This should be validated
                    String pathOrigin = args[0];

					FileInputStream fin = new FileInputStream(pathOrigin);
				    BufferedInputStream bis = new BufferedInputStream(fin);
    				CompressorInputStream input = new CompressorStreamFactory().createCompressorInputStream(bis);
    				BufferedReader br2 = new BufferedReader(new InputStreamReader(input));
                    // TODO: This should be validated
			 		// replacing .json.bz2 --> _enc.json.bz2
					OutputStream fout = new FileOutputStream(pathOrigin.replace(".json.bz2","_enc.json.bz2"));
					OutputStream bzip2 = new BZip2CompressorOutputStream(fout);

					String line = "";
			 		int rowNum = 0;

    				while ((line = br2.readLine()) != null) {
    					rowNum++;
    						
    					//System.out.println(line);
    					// Loading JSON to Class using gson
    					FtBolVenta ftbv = gson.fromJson(line, FtBolVenta.class);
    					// Creating encryptor /with fixed password and salt
				        TextEncryptor encryptor = Encryptors.queryableText(password, salt);      

				        // this should be a setter method, only encrypt if is not null
				        // and obviously we should create an iterator

        				if (ftbv.NUM_RUT != null && !ftbv.NUM_RUT.isEmpty())
        					ftbv.NUM_RUT = encryptor.encrypt(ftbv.NUM_RUT);
   						
   						if (ftbv.DV_RUT != null && !ftbv.DV_RUT.isEmpty())
   							ftbv.DV_RUT = encryptor.encrypt(ftbv.DV_RUT);

   						if (ftbv.NUM_RUT_COMPRADOR != null && !ftbv.NUM_RUT_COMPRADOR.isEmpty())
   							ftbv.NUM_RUT_COMPRADOR = encryptor.encrypt(ftbv.NUM_RUT_COMPRADOR);

   						if (ftbv.DV_RUT_COMPRADOR != null && !ftbv.DV_RUT_COMPRADOR.isEmpty())
				        	ftbv.DV_RUT_COMPRADOR = encryptor.encrypt(ftbv.DV_RUT_COMPRADOR);

        				// Could reuse encryptor but wanted to show reconstructing TextEncryptor
				        // TextEncryptor decryptor = Encryptors.queryableText(password, salt);
				        // String decryptedText = decryptor.decrypt(encryptedText);
				        
				        String jsonInString = gson.toJson(ftbv)+"\n";

				        //System.out.println(jsonInString);

				        bzip2.write(jsonInString.getBytes());

    				}


    				bzip2.close();
					br2.close();
    				input.close();
    				bis.close();
    				fin.close();

		        } catch (Exception e) {
		            e.printStackTrace();
		        }


    }
}


/*
{"MONTO_DESCTO_US":0,"MONTO_ABONO_US":0,"NCORRELATIVO":2,"ID_CUOTAS":0,"NUM_RUT_COMPRADOR":6361656,"MONTO_IVA_2":0,"NBIN_TARJETA":0,"MONTO_IVA_1":-956,"NUM_BOLETA":9503,"MONTO_IVA_8":null,"VALOR_CUOTA_UF":null,"MONTO_IVA_7":null,"MONTO_IVA_9":0,"ID_LOCAL_OMNICANAL":3296,"MONTO_DESCTO_UF":0,"MONTO_IVA_4":0,"CEVENTO_NOVIOS":null,"NUM_F12":0,"VALOR_CUOTA":null,"PRD_LVL_CHILD":7301796,"MONTO_IVA_3":0,"MONTO_IVA_6":null,"ID_TRANSAC_NC":null,"ID_HORA":1316,"MONTO_IVA_5":0,"NORDENCMP":0,"VALOR_CUOTA_US":0,"ID_METODO_DESPACHO":0,"PRECIO_ORIGINAL":-12990,"MONTO_LIQUID_UF_M2XMUNDO":-0.000158,"NUM_CUENTA":32962742009503181101131639,"CTRANSAC":35,"MONTO_BRUTO_US":-8.639714,"ID_NUM_CUOTAS":null,"TK_VENDEDOR":418046,"MONTO_BRUTO_UF":-0.218336,"DCOMPRA":2018-11-01,"MONTO_ABONO_UF":0,"CDOCUMENTO":null,"VALOR_BONIFICA":0,"PIVA":19,"ID_DIA":2018-11-01,"PRECIO_COSTO":-2977.69,"MONTO_BRUTO":-5990,"VALOR_COMPLEMENTO":0,"FLAG_BANDA":null,"ID_NUMERO_TRN":800582295,"FLAG_DIFER":null,"VALOR_ACORTAMIENTO":0,"ID_TIPOF12":"0","BMATCH":"2","ID_TRANSAC":14,"CIMEI":null,"NUM_COLEGIO":null,"ID_SECTOR":null,"CBANDA":null,"TRN_TECH_KEY":1501728219,"DV_RUT_COMPRADOR":"7","MONTO_DESCTO":0,"COD_DESCUENTO":"-2","CANT_VENTA":-1,"ID_POS":2742,"ID_TITULAR":null,"ID_EAN":2998811631576,"PRECIO_COSTO_REP":-2977.69,"ID_CANAL_VENTA":0,"IVA_VENTA":-956,"ID_TIEMPO":19,"NUM_RUT":null,"MONTO_LIQUID_UF_M2XTIENDA":-0.00003,"ID_CANAL_VTA_CUBO":0,"MONTO_INGRESO_BRUTO":0,"CODIGO_VENTA":null,"MONTO_LIQUID":-5990,"NUM_VENDEDOR":577928,"MONTO_LIQUID_UF":-0.218336,"FLAG_VTA_VERDE":0,"NUM_VENDEDOR_CAJERO":null,"DISABLE":null,"MONTO_LIQUID_US":-8.639714,"COD_OPUNICA":null,"PRECIO_NORMAL_US":18.736207,"IVA_ORIGINAL":-2074,"ID_SKU":881163157,"FLAG_PRECIO_MAXIMO":null,"PRECIO_COSTO_US":-4.29489,"SIC":null,"PRECIO_NORMAL_UF":0.473487,"IVA_VENTA_US":-1.378893,"BPOR":"F","PRECIO_COSTO_UF":-0.108537,"DV_RUT":null,"NUM_PAM":null,"QTICKET_CAMBIO":0,"COD_AUTORIZA":null,"MONTO_ABONO":0,"VALOR_ACORTAMIENTO_UF":null,"IVA_VENTA_UF":-0.034846,"FLAG_INTERNET":"O","PRECIO_NORMAL":12990,"NUM_SII":557877311,"FECHA_REAL":2018-11-01,"DESCUENTO_IVA":0,"CONTRATO":null,"ID_CONCILIO":null,"ID_VENDEDOR_TIENDA":null,"FLAG_ENLINEA":null,"NCAJERO":157848,"FLAG_NOTA_DEBITO":null,"FLAG_FACTURA":0,"VALOR_MERCADERIA":0,"ID_AUTORIZA":null,"ID_NOVIOS":null,"ID_HUELLA":0,"ID_EVENTO":"-1","ID_LOCAL":3296,"ID_EMPRESA":null,"PRECIO_COSTO_V":null,"CPROMOCION":null,"ID_BOLETA":1614330278,"FLAG_RECAUDACION":null,"ID_PAIS_PAN":"X"}
*/
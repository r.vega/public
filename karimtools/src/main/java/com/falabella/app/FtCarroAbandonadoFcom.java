package com.falabella.app;

import java.sql.Date;

public class FtCarroAbandonadoFcom {

	/*	
	NUM_RUT NUMBER,
	ID_DMC_CLIENTE NUMBER,
	EMAIL VARCHAR2(4000),
	ID_SKU VARCHAR2(4000),
	FECHA_CREACION DATE,
	FECHA_ULTIMA_MODIFICACION DATE,
	FECHA_PROCESO DATE
	*/
	
	public int NUM_RUT;
	public int ID_DMC_CLIENTE;
	public String EMAIL;
	public String ID_SKU;
	public Date FECHA_CREACION;
	public Date FECHA_ULTIMA_MODIFICACION;
	public Date FECHA_PROCESO;
		
}
readme.md

brew install maven@3.6

mvn -B archetype:generate \
  -DarchetypeGroupId=org.apache.maven.archetypes \
  -DgroupId=com.falabella.app \
  -DartifactId=dataconn

-- After we installed, we changed UTF-8 encoding and Build properties

-- Run
mvn verify

-- Test
mvn test

-- Build
mvn clean package

-- Run
mvn exec:java -Dexec.mainClass="com.falabella.app.JsonCrypt" -Dexec.args="/Users/karintouma/Desktop/test_ft_bol_venta/FT_BOL_VENTA_20181113.json.bz2"

